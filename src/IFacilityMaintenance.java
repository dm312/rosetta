public interface IFacilityMaintenance{	

	public Object makeFacilityMaintRequest();
	public Object scheduleMaintenance(int log);
	public Object calcMaintenanceCostForFacility(Facility faci);
	public Object calcProblemRateForFacility(Facility faci);
	public Object calcDownTimeForFacility();
	public Object listMaintRequests();
	public Object listMaintenance();
	public Object listFacilityProblems();

}