/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Daniel
 */
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
public class ConnectPanel extends JPanel{
    private JPanel row4, row5;
    private JButton connect, disconnect, add, delete, update, search, clear; 
    private JLabel jLabel1, jLabel2, jLabel3;
    private JTextField facnumbertxt, hidden_id, faccapacitytxt, facdetailtxt;
    private Connection con = null; 
    
    public ConnectPanel(){
        inItComponents();
    }
    public void inItComponents(){
        connect = new JButton();
        disconnect = new JButton();
        add = new JButton();
        delete = new JButton();
        search = new JButton();
        update = new JButton();
        clear = new JButton(); 
        jLabel1 = new JLabel();
        jLabel2 = new JLabel();
        jLabel3 = new JLabel(); 
        facnumbertxt = new JTextField();
        faccapacitytxt = new JTextField();
        facdetailtxt = new JTextField();
        hidden_id = new JTextField();
              
        connect.setText("Connect");
        disconnect.setText("Disconnect");
        add.setText("Add");
        delete.setText("Delete");
        update.setText("Update");
        search.setText("Search");
        clear.setText("Clear");
        
        disconnect.setEnabled(false);
        add.setEnabled(false);
        search.setEnabled(false);
        update.setEnabled(false);
        search.setEnabled(false);
        clear.setEnabled(false);
        delete.setEnabled(false);
        
        hidden_id.setEditable(false);
        facnumbertxt.setEditable(false);
        facdetailtxt.setEditable(false);
        faccapacitytxt.setEditable(false);
        
        facnumbertxt.setColumns(13);
        facdetailtxt.setColumns(13);
        faccapacitytxt.setColumns(13);
        hidden_id.setColumns(5);
        
        jLabel1.setText("Facility Number   :");
        jLabel2.setText("Facility Detail       :");
        jLabel3.setText("Facility Capacity :");
        
        connect.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
            MyFunctions test = new MyFunctions();
                con = test.global_con();
                try{
                    if(con.isValid(con.getNetworkTimeout()))
                    {
                        enabled(true,false);
                        JOptionPane.showMessageDialog(null, "Connected.");
                        
                    }
                }
                //Do not modify 
                catch(Exception se)
                {}            }
        });
     
        disconnect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                con = null;
                enabled(false, true);
                clearFields();
                JOptionPane.showMessageDialog(null, "Disconnected.");
            }
        });
        add.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                        
                if (add.getText().equals("Add")) {
                    add.setText("Save");
                    clearFields();
                } else {
                    String err = validated();
                    if (!err.equals("")) {
                        JOptionPane.showMessageDialog(null, err);
                    }
                    else{
                        String exist = exist("");
                        if(exist.equals("0")){
                            try {
                                Statement st = con.createStatement();
                                if(faccapacitytxt.getText().equals(""))
                                    faccapacitytxt.setText("-1");
                                st.executeUpdate("Insert into facility (facility_number, facility_detail, facility_capacity) values ('"
                                        + facnumbertxt.getText() + "','" + facdetailtxt.getText() + "','" + faccapacitytxt.getText() + "')");
                                JOptionPane.showMessageDialog(null, "Record Saved.");
                                clearFields();
                               
                                facnumbertxt.requestFocus();
                                add.setText("Add");
                            } catch (Exception se) {
                                JOptionPane.showMessageDialog(null, se.getMessage());
                            }
                        }
                        else if(exist.equals("1")){
                            JOptionPane.showMessageDialog(null, "Facility Number already exists.");
                            facnumbertxt.requestFocus();
                        }
                    }
                }
            }
        });
        search.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                add.setText("Add");
                facdetailtxt.setText("");
                faccapacitytxt.setText("");
                hidden_id.setText("");
                if (facnumbertxt.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Enter Facility Number");
                    facnumbertxt.requestFocus();
                } else {
                    try {
                        Statement st = con.createStatement();
                        st.executeQuery("Select * from facility where facility_number = '" + facnumbertxt.getText() + "'");
                        ResultSet rs = st.getResultSet();

                        if (rs.next() == false) {
                            JOptionPane.showMessageDialog(null, "No record found.");
                            facnumbertxt.requestFocus();
                        } else {
                            rs.beforeFirst();
                            while(rs.next())
                            {
                            facnumbertxt.setText(rs.getString("facility_number"));
                            faccapacitytxt.setText(rs.getString("facility_capacity"));
                            facdetailtxt.setText(rs.getString("facility_detail"));
                            hidden_id.setText(rs.getString("facility_number"));
                            }
                        }
                    } catch (Exception se) {
                        JOptionPane.showMessageDialog(null, se.getMessage());
                    }
                }
            }
        });
        delete.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(hidden_id.getText().equals("")){
                    JOptionPane.showMessageDialog(null,"Search the facility to be deleted.");
                }    
                else{
                    try{
                        int answer = JOptionPane.showConfirmDialog(null,"Are you sure you want to delete this record?", "Message",JOptionPane.YES_NO_OPTION);
                        if(answer == JOptionPane.YES_OPTION){
                        Statement stmt = con.createStatement();
                        stmt.executeUpdate("Delete from facility where facility_number = '" + hidden_id.getText() + "'");
                        JOptionPane.showMessageDialog(null,"Record Deleted.");
                        clearFields();
                        facnumbertxt.requestFocus();
                        }
                    }
                    catch(Exception se){
                        JOptionPane.showMessageDialog(null, se.getMessage());
                    }
                }
                
            }
        });
        update.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                add.setText("Add");
                String err = validated();
                if(hidden_id.getText().equals(""))
                {
                    JOptionPane.showMessageDialog(null,"Search the facility you want to update.");
                }
                else if(!err.equals(""))
                {
                    JOptionPane.showMessageDialog(null, err);
                }
                else
                {
                    String exist = exist("and facility_number != '" + hidden_id.getText() + "'");
                    if (exist.equals("0")) {
                        try 
                        {
                            if (faccapacitytxt.getText().equals("")) {
                                faccapacitytxt.setText("-1");
                            }
                            Statement st = con.createStatement();
                            st.executeUpdate("Update facility set facility_number = '" + facnumbertxt.getText() + "', facility_detail = '" + facdetailtxt.getText() + "', facility_capacity = '" + faccapacitytxt.getText() + "' where facility_number = '" + hidden_id.getText() + "'");
                            JOptionPane.showMessageDialog(null, "Record Updated.");
                        } 
                        catch (Exception se) 
                        {
                            JOptionPane.showMessageDialog(null, se.getMessage());
                            facnumbertxt.requestFocus();
                        }
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "Facility Number already exists.");
                        facnumbertxt.requestFocus();
                    }
                }
            }
        });
        clear.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                add.setText("Add");
                facnumbertxt.requestFocus();
                clearFields();
            }
        });
        BoxLayout bl = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(bl);
        JPanel row1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel row2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel row3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        row1.add(Box.createHorizontalStrut(18));
        row1.add(connect);
        row1.add(Box.createHorizontalStrut(4));
        row1.add(disconnect);
        row1.add(Box.createVerticalStrut(9));

        row2.add(Box.createHorizontalStrut(18));
        row2.add(jLabel1);
        row2.add(facnumbertxt);
        row2.add(Box.createHorizontalStrut(9));
        row2.add(search);

        //Hidden row 3  
        row4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        row4.add(Box.createHorizontalStrut(18));

        row4.add(jLabel2);
        row4.add(facdetailtxt);
        row4.add(Box.createHorizontalStrut(12));
        row4.add(hidden_id);
        row4.setVisible(false);
        //End of hidden row 3 
        
        //Hidden row 4 
        row5 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        row5.add(Box.createHorizontalStrut(18));
        row5.add(jLabel3);
        row5.add(faccapacitytxt);
        row5.setVisible(false);
        //End of hidden row 4
        
        row3.add(Box.createHorizontalStrut(18));
        row3.add(Box.createVerticalStrut(4));
        row3.add(add);
        row3.add(Box.createHorizontalStrut(4));
        row3.add(update);
        row3.add(Box.createHorizontalStrut(4));
        row3.add(delete);
        row3.add(Box.createHorizontalStrut(4));
        row3.add(clear);
       
        this.add(Box.createVerticalStrut(18));
        
        add(row1);
        add(row2);
        add(row4);
        add(row5);
        add(row3);
        this.add(Box.createVerticalStrut(60));
     
        }
    
    public JButton getConnect(){
        return this.connect;
    }
    public JButton getDisconnect(){
        return this.disconnect;
    }
    public JButton getAdd(){
        return this.add;
    }
    public JButton getDelete(){
        return this.delete;
    }
    public JButton getUpdate(){
        return this.update;
    }
    public JButton getSearch(){
        return this.search;
    }
    public JButton getClear(){
        return this.clear;
    }
    private void enabled(boolean type, boolean type2){
        row4.setVisible(type);
        row5.setVisible(type);
        hidden_id.setVisible(type);
        
        hidden_id.setEditable(type);
        facnumbertxt.setEditable(type);
        facdetailtxt.setEditable(type);
        faccapacitytxt.setEditable(type);
        search.setEnabled(type);
        add.setEnabled(type);
        update.setEnabled(type);
        delete.setEnabled(type);
        clear.setEnabled(type);
        disconnect.setEnabled(type);
        connect.setEnabled(type2);
        

        
    }
    public String validated(){
        String err = "";
        if(facnumbertxt.getText().equals("")){
            err = "Facility Number is required";
            facnumbertxt.requestFocus();
        } 
            return err;
    }
     public String exist(String param)
    {
        String ret = "0";
        try
          {
              Statement st = con.createStatement();
              st.executeQuery("Select * from facility where facility_number = '"+facnumbertxt.getText()+"' "+param);
              ResultSet rs = st.getResultSet();
              if(rs.next() == true)
              {
                  ret = "1";
              }
          }
          catch(Exception e)
          {
              ret = "error";
              JOptionPane.showMessageDialog(null,e.getMessage());
          }  
        return ret;
    }
    private void clearFields(){
        facnumbertxt.setText("");
        facdetailtxt.setText("");
        faccapacitytxt.setText("");
        hidden_id.setText("");
    }
  
    public static void main(String [] args){
        JFrame frame = new JFrame();
        frame.add(new ConnectPanel());
        frame.setVisible(true);
        frame.pack();

    }
    
}
