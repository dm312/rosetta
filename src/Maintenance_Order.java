/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Daniel
 */
public class Maintenance_Order {
    
    public String getOrder(){
        return this.order;
    }
    public Maintenance_Order_Cost getCost(){
        return this.cost;
    }
    public void setOrder(String order){
        this.order = order;
    }
    public void setMaintenance_Order_Cost(Maintenance_Order_Cost cost){
        this.cost = cost;
    }
  
    private String order; 
    private Maintenance_Order_Cost cost;
    private int mNumber;
}
