
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class FacilityDAO {

    public FacilityDAO() {
    }
    //TODO Remove double slash on line 13 when Facility --> getFacility is implemented

    public Facility getFacility(int number) {

        try {
            DBHelper test = new DBHelper();
            Statement st = test.getConnection().createStatement();
            String selectFacilityQuery = "SELECT number, capacity FROM Facility WHERE number = '" + number + "'";
            ResultSet factrs = st.executeQuery(selectFacilityQuery);
            System.out.println("FacilityDAO: *************** Query " + selectFacilityQuery);

            //Get Facility
            Facility host = new Facility();
            while (factrs.next()) {
                host.setNumber(factrs.getInt("facility_number"));
                host.setCapacity(factrs.getInt("facility_capacity"));

            }
            String selectFacility_DetailQuery = "SELECT character FROM Facility_Detail where number = '" + number + "'";
            ResultSet rs = st.executeQuery(selectFacility_DetailQuery);
            System.out.println("FacilityDAO: ************* Query " + selectFacility_DetailQuery);

            //Get Facility_Detail
            Facility_Detail host2 = null;
            while (rs.next()) {
                host2.setCharacter(rs.getString("character"));

            }
            host.setDetail(host2);
            //close to manage resources
            factrs.close();

            /*//TODO does Maintenance and Facility go hand-in-hand //note it doesn't matter
			//Get Maintenance
			String selectFacilityMaintenanceQuery = "SELECT workBoard, facilityNumber FROM Maintenance WHERE facility_Number = '" + number + "'";
			ResultSet maintrs = st.getResultSet();

			Maintenance oatmeal = new Maintenance(); 

			System.out.println("FacilityDAO: *************** Query " + selectFacilityMaintenanceQuery);
			while(maintrs.next()){

			}

			//host.setFacilityMaintenance(upkeep);
			//close to manage resources


			//return host;
             */
            st.close();
            return host;
        } catch (SQLException se) {
            System.out.println("DBHelper: Threw an SQL Exception trying to get a Facility");
            System.out.println(se.getMessage());
            se.printStackTrace();

        }
        return null;
    }
    public Use getUse(int number) {

        try {
            DBHelper test = new DBHelper();
            Statement st = test.getConnection().createStatement();
            String selectUseQuery = "SELECT fee, useNumber, fNumber, mNumber FROM use WHERE number = '" + number + "'";
            ResultSet factrs = st.executeQuery(selectUseQuery);
            System.out.println("FacilityDAO: *************** Query " + selectUseQuery);

            //Get Use
            Use host = new Use();
            while (factrs.next()) {
              host.setFee(factrs.getInt("fee"));
              host.setFee(factrs.getInt("useNnumber"));
              host.setFee(factrs.getInt("fNumber"));
              host.setFee(factrs.getInt("mNumber"));
            }
            
            String selectFacility_DetailQuery = "SELECT * FROM Inspections where fNumber = '" + number + "'";
            ResultSet rs = st.executeQuery(selectFacility_DetailQuery);
            System.out.println("FacilityDAO: ************* Query " + selectUseQuery);
            
            //Get Inspections
            Inspections temporary = new Inspections();
            while(factrs.next()){
                temporary.setEmergencyPreparedness(factrs.getBoolean("emergencyPreparedness"));
                temporary.setFireProtection(factrs.getBoolean("fireProtection"));
                temporary.setHouseKeeping(factrs.getBoolean("houseKeeping"));
                temporary.setFNumber(factrs.getInt("fNumber"));
            }
            host.setChecks(temporary);
            //End of Inspections 
            //End of Use
            //close to manage resources
            factrs.close();
            st.close();
            return host;
        } catch (SQLException se) {
            System.out.println("DBHelper: Threw an SQL Exception trying to get a Use");
            System.out.println(se.getMessage());
            se.printStackTrace();

        }
        return null;
    }
    @SuppressWarnings("unchecked")
    public Maintenance getMaintenance(int number) {
        try {
            DBHelper test = new DBHelper();
            Statement st = test.getConnection().createStatement();
            String selectFacilityMaintenanceQuery = "SELECT * FROM Maintenance WHERE facility_Number = '" + number + "'";
            ResultSet factrs = st.executeQuery(selectFacilityMaintenanceQuery);
            System.out.println("FacilityDAO: *************** Query " + selectFacilityMaintenanceQuery);

            //Get Maintenance
            Maintenance host = new Maintenance();
            while (factrs.next()) {
                host.setFNumber(number);
                host.setMaintenanceNumber(factrs.getInt("maintenanceNumber"));
                host.setUNumber(factrs.getInt("uNumber"));

            }

            //Get WorkBoard
            String selectMaintenancyWBQuery = "SELECT * From Maintenance_Request where ";
            ResultSet factors = st.executeQuery(selectMaintenancyWBQuery);
            System.out.println("FacilityDAO: ************* Query" + selectMaintenancyWBQuery);

            ArrayList<Maintenance_Request> temporary = new ArrayList<Maintenance_Request>();
            while (factors.next()) {
                Maintenance_Request temporary2 = new Maintenance_Request();
                temporary2.setProblem(factors.getBoolean("problem"));
                temporary2.setIncidentReport(factors.getString("incidentReport"));
                temporary2.setExpense(factors.getInt("expense"));
                temporary2.setLog(factors.getInt("Maintenance_Log"));
                temporary2.setMNumber(factors.getInt("mNumber"));
                temporary.add(temporary2);
            }
            
            //Set Workboard in Maintenance 
            host.setWorkBoard(temporary);
            //End of Set Workboard in Maintenance 
            
            //End of Get WorkBoard
            //close to manage resources
            factrs.close();
            st.close();
            return host;
        } 
        
        catch (SQLException se) {
            System.out.println("DBHelper: Threw an SQL Exception trying to get a Maintenance");
            System.out.println(se.getMessage());
            se.printStackTrace();
        }
        return null;

    }

    public ArrayList<Facility> getAllFacility() {
        try {
            DBHelper test = new DBHelper();
            Statement st = test.getConnection().createStatement();
            String selectAllFacilityQuery = "SELECT * FROM Facility";
            ResultSet factrs = st.executeQuery(selectAllFacilityQuery);
            System.out.println("FacilityDAO: *************** Query " + selectAllFacilityQuery);

            //Get Facility
            ArrayList<Facility> FacilityList = new ArrayList<Facility>();
            Facility host = new Facility();
            while (factrs.next()) {
                host.setNumber(factrs.getInt("number"));
                host.setCapacity(factrs.getInt("capacity"));
                host.setDetail((Facility_Detail) factrs.getObject("detail"));

                FacilityList.add(host);
            }
            //close to manage resources
            factrs.close();
            return FacilityList;

        } catch (SQLException se) {
            System.out.println("DBHelper: Threw an SQL Exception trying to get all Facility");
            System.out.println(se.getMessage());
            se.printStackTrace();

        }
        return null;
    }

    //TODO think about adding IFacility as an interface , not a concrete class 
    public void addFacility(IFacility faci) {
        DBHelper test = new DBHelper();
        Connection con = test.getConnection();
        PreparedStatement factPst = null;
        PreparedStatement addPst = null;

        try {
            //Insert the Facility object
            String factStm = "INSERT INTO facility(number, capacity, detail) values(?, ?, ?)";
            factPst = con.prepareStatement(factStm);
            factPst.setInt(1, faci.getNumber());
            factPst.setInt(2, faci.getCapacity());
            factPst.setObject(3, faci.getDetail());
            factPst.executeUpdate();

            /*//Insert the FacilityMaintenance object
			String addStm = "INSERT INTO FacilityMaintenance(x,y,z) values(?, ?, ?)"; 
			addPst = con.prepareStatement(addStm);
			//addPst.setString(1, faci.getFacilityInformation().getX());
			addPst.executeUpdate();
             */
        } catch (SQLException se) {
            System.out.println("DBHelper: Error inserting Facility object");
            System.out.println(se.getMessage());
            se.printStackTrace();
        } finally {
            try {
                if (addPst != null) {
                    factPst.close();
                    addPst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                System.err.println("DBHelper: Threw a SQLException saving the facility object.");
                System.err.println(ex.getMessage());
            }
        }
    }

    public void addMaintenance(Maintenance main) {
        DBHelper test = new DBHelper();

        Connection con = test.getConnection();
        PreparedStatement factPst = null;
        PreparedStatement addPst = null;

        try {
            //Insert the Maintenance object
            String factStm = "INSERT INTO maintenance (fnumber, workboard, maintenanceNumber) values(?, ?, ?)";
            factPst = con.prepareStatement(factStm);
            factPst.setInt(1, main.getFNumber());
            factPst.setObject(2, main.getWorkBoard());
            factPst.setInt(3, main.getMaintenanceNumber());
            factPst.setInt(4, main.getUNumber());
            factPst.executeUpdate();

        } catch (SQLException se) {
            System.out.println("DBHelper: Error inserting Maintenance object");
            System.out.println(se.getMessage());
            se.printStackTrace();
        } finally {
            try {
                if (addPst != null) {
                    factPst.close();
                    addPst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                System.err.println("DBHelper: Threw a SQLException saving the maintenance object.");
                System.err.println(ex.getMessage());
            }
        }
    }
}
