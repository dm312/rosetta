/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Daniel
 */
import java.sql.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
 
public class Screen extends JFrame {


    public Screen() {
        initComponents();
    }
    
    private void initComponents() {
        JFrame frame = new JFrame();
        JTabbedPane bar = new JTabbedPane(4);
        
        JPanel cards = new JPanel();
        CardLayout cl = new CardLayout();
        cards.setLayout(cl);

        bar.setTabPlacement(JTabbedPane.TOP);
        
        //Login Panel
        JPanel loginPanel = new JPanel();
        BoxLayout z = new BoxLayout(loginPanel, BoxLayout.Y_AXIS);
        loginPanel.setLayout(z);
        //end of Login Panel
        //Connect Panel
        JPanel connectPanel = new JPanel();
        //<editor-fold defaultstate="collapsed" desc="comment">
        
        connect = new JButton();
        disconnect = new JButton();
        add = new JButton();
        delete = new JButton();
        
        search = new JButton();
        update = new JButton();
        clear = new JButton(); 
        listFacilities = new JButton();
        available = new JButton();
        jLabel1 = new JLabel();
        jLabel2 = new JLabel();
        jLabel3 = new JLabel(); 
        facnumbertxt = new JTextField();
        faccapacitytxt = new JTextField();
        facdetailtxt = new JTextField();
        hidden_id = new JTextField();
        
        connect.setText("Connect");
        disconnect.setText("Disconnect");
        add.setText("Add");
        delete.setText("Delete");
        update.setText("Update");
        search.setText("Search");
        clear.setText("Clear");
        listFacilities.setText("List Facilities");
        available.setText("Check availability");
        
        disconnect.setEnabled(false);
        add.setEnabled(false);
        search.setEnabled(false);
        update.setEnabled(false);
        search.setEnabled(false);
        clear.setEnabled(false);
        delete.setEnabled(false);
        listFacilities.setEnabled(false);
        available.setEnabled(false);
        
        hidden_id.setEditable(false);
        facnumbertxt.setEditable(false);
        facdetailtxt.setEditable(false);
        faccapacitytxt.setEditable(false);
        
        facnumbertxt.setColumns(13);
        facdetailtxt.setColumns(13);
        faccapacitytxt.setColumns(13);
        hidden_id.setColumns(5);
        
        jLabel1.setText("Facility Number   :");
        jLabel2.setText("Facility Detail       :");
        jLabel3.setText("Facility Capacity :");
        connect.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                connectActionPerformed(e);
            }
        });
       
        disconnect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               disconnectActionPerformed(e);
            }
        });
        add.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                addActionPerformed(e);
            }
        });
        search.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                searchActionPerformed(e);
            }
        });
        delete.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                deleteActionPerformed(e);
            }
        });
        update.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                updateActionPerformed(e);
            }
        });
        clear.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                clearActionPerformed(e);
            }
        });
        listFacilities.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                listFacilitiesActionPerformed(e);
            }
        });
        BoxLayout bl = new BoxLayout(connectPanel, BoxLayout.Y_AXIS);
        connectPanel.setLayout(bl);
        JPanel row1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel row2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel row3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        row1.add(Box.createHorizontalStrut(18));
        row1.add(connect);
        row1.add(Box.createHorizontalStrut(4));
        row1.add(disconnect);
        row1.add(Box.createVerticalStrut(9));

        row2.add(Box.createHorizontalStrut(18));
        row2.add(jLabel1);
        row2.add(facnumbertxt);
        row2.add(Box.createHorizontalStrut(9));
        row2.add(search);

        //Hidden row 3  
        row4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        row4.add(Box.createHorizontalStrut(18));

        row4.add(jLabel2);
        row4.add(facdetailtxt);
        row4.add(Box.createHorizontalStrut(12));
        row4.add(hidden_id);
        row4.setVisible(false);
        //End of hidden row 3 
        
         //Hidden row 4 
        row5 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        row5.add(Box.createHorizontalStrut(18));
        row5.add(jLabel3);
        row5.add(faccapacitytxt);
        row5.setVisible(false);
        //End of hidden row 4
        
        row3.add(Box.createHorizontalStrut(18));
        row3.add(Box.createVerticalStrut(4));
        row3.add(add);
        row3.add(Box.createHorizontalStrut(4));
        row3.add(update);
        row3.add(Box.createHorizontalStrut(4));
        row3.add(delete);
        row3.add(Box.createHorizontalStrut(4));
        row3.add(clear);
        row3.add(Box.createHorizontalStrut(4));
        row3.add(listFacilities);
      
        connectPanel.add(Box.createVerticalStrut(18));
        
        connectPanel.add(row1);
        connectPanel.add(row2);
        connectPanel.add(row4);
        connectPanel.add(row5);
        connectPanel.add(row3);
        connectPanel.add(Box.createVerticalStrut(60));
    //</editor-fold>

        //end of Connect Panel
        
        //Use Panel
        //<editor-fold defaultstate="collapsed" desc="comment">
        JPanel usePanel = new JPanel();
        BoxLayout bl2 = new BoxLayout(usePanel, BoxLayout.Y_AXIS);
        usePanel.setLayout(bl2);
        
        jLabel4 = new JLabel();
        jLabel5 = new JLabel();
        jLabel6 = new JLabel();
        jLabel7 = new JLabel();
        jLabel8 = new JLabel();
        jLabel9 = new JLabel();
        jLabel10 = new JLabel();
        
        facnotxt = new JTextField();
        vacanttxt = new JTextField();
        hidden_id2 = new JTextField();
        feetxt = new JTextField();
        starttxt = new JTextField();
        endtxt = new JTextField();
        
        isInUseDuringInterval = new JButton(); 
        assignFacility = new JButton(); 
        vacate = new JButton(); 
        listInspections = new JButton(); 
        listUsage = new JButton(); 
        calcUsageRate = new JButton(); 

        useUpdate = new JButton();
        useDelete = new JButton();
        useClear = new JButton();
        useSearch = new JButton();
  
        hidden_id2.setVisible(false);
        
        useSearch.setText("Search");
        useClear.setText("Clear");
        useDelete.setText("Delete");
        useUpdate.setText("Update");
        
        jLabel4.setText("Facility Number   :");
        jLabel5.setText("Vacant                   :");
        jLabel6.setText("Fee                         :");
        jLabel7.setText("Check Availability:");
        jLabel9.setText("From              :");
        jLabel10.setText("To               :");
        
        isInUseDuringInterval.setText("Check Availability");
        assignFacility.setText("Assign Facility");
        vacate.setText("Vacate");
        listInspections.setText("List Inspections");
        listUsage.setText("List Usage");
        calcUsageRate.setText("Calculate Actual Usage Rate");
        
        facnotxt.setColumns(13);
        hidden_id2.setColumns(5);
        vacanttxt.setColumns(13);
        feetxt.setColumns(13);
        starttxt.setColumns(9);
        endtxt.setColumns(9);
        
        facnotxt.setEditable(false);
        hidden_id2.setEditable(false);
        vacanttxt.setEditable(false);
        feetxt.setEditable(false);
        starttxt.setEditable(false);
        endtxt.setEditable(false);
        
        useSearch.setEnabled(false);
        useUpdate.setEnabled(false);
        useSearch.setEnabled(false);
        useClear.setEnabled(false);
        useDelete.setEnabled(false);
        isInUseDuringInterval.setEnabled(false);
        assignFacility.setEnabled(false);
        vacate.setEnabled(false);
        listInspections.setEnabled(false);
        listUsage.setEnabled(false);
        calcUsageRate.setEnabled(false);
        
        useClear.addActionListener(new ActionListener()
        {
           public void actionPerformed(ActionEvent e)
            {
                useClearActionPerformed(e);
            } 
        });
        useDelete.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
           {
                useDeleteActionPerformed(e);
            }
        });
        useSearch.addActionListener(new ActionListener()
        {
           public void actionPerformed(ActionEvent e)
           {
               useSearchActionPerformed(e);
           }
        });
        vacate.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                vacateActionPerformed(e); 
            }
        });
        assignFacility.addActionListener(new ActionListener()
        {
           public void actionPerformed(ActionEvent e)
           {
               assignFacilityActionPerformed(e);
           }
        });
        useUpdate.addActionListener(new ActionListener()
        {
           public void actionPerformed(ActionEvent e)
           {
               useUpdateActionPerformed(e);
           }
        });
        listInspections.addActionListener(new ActionListener()
        {
           public void actionPerformed(ActionEvent e)
           {
               listInspectionsActionPerformed(e);
           }
        });
        listUsage.addActionListener(new ActionListener()
        {
           public void actionPerformed(ActionEvent e)
           {
               listUsageActionPerformed(e);
           }
        });
        calcUsageRate.addActionListener(new ActionListener()
        {
           public void actionPerformed(ActionEvent e)
           {
               calcUsageRateActionPerformed(e);
           }
        });
        available.addActionListener(new ActionListener()
        {
           public void actionPerformed(ActionEvent e)
           {
               checkAvailableActionPerformed(e);
           }
        });
        JPanel row6 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        row6.add(assignFacility);
        row6.add(vacate);
        
        JPanel row7 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        row7.add(jLabel4);
        row7.add(facnotxt);
        row7.add(useSearch);
        
        JPanel row8 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        row8.add(jLabel5);
        row8.add(vacanttxt);
        row8.add(hidden_id2);
        
        JPanel row9 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        row9.add(jLabel6);
        row9.add(feetxt);
        
        JPanel row10 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        row10.add(jLabel7);
        row10.add(jLabel9);
        row10.add(starttxt);
        row10.add(jLabel10);
        row10.add(endtxt);
        
        JPanel row11 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        row11.add(useUpdate);
        row11.add(useDelete);
        row11.add(useClear);
        row11.add(listInspections);
        row11.add(listUsage);
        row11.add(calcUsageRate);
        
        JPanel row12 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel help = new JLabel();
        help.setText("Format of Entered Dates: yyyy-mm-ddThh:mm:ss");
        row12.add(available);
        row12.add(help);
        
        usePanel.add(row6);
        usePanel.add(row7);
        usePanel.add(row8);
        usePanel.add(row9);
        usePanel.add(row10);
        usePanel.add(row11);
        
        usePanel.add(row12);
        //</editor-fold>
        //End of Use Panel
        //Maintenance Panel
        JPanel maintenancePanel = new JPanel();
        //<editor-fold>
        BoxLayout bl3 = new BoxLayout(maintenancePanel, BoxLayout.Y_AXIS);
        maintenancePanel.setLayout(bl3);
        maekRequest = new JButton();
        listMaintenance = new JButton();
        maintenanceSelect = new JButton();
        listMaintRequests = new JButton();
        listFacilityProblems = new JButton();
        scheduleMaintenance = new JButton();
        maintenanceCost = new JButton();
        problemRate = new JButton();
        downtimeRate = new JButton();
        
        
        maekRequest.setEnabled(false);
        listMaintenance.setEnabled(false);
        maintenanceSelect.setEnabled(false);
        listMaintRequests.setEnabled(false);
        listFacilityProblems.setEnabled(false);
        scheduleMaintenance.setEnabled(false);
        maintenanceCost.setEnabled(false);
        problemRate.setEnabled(false);
        downtimeRate.setEnabled(false);
        
        maekRequest.setText("Maek Request");
        listMaintenance.setText("List Maintenance");
        maintenanceSelect.setText("Select");
        listMaintRequests.setText("List Maintenance Requests");
        listFacilityProblems.setText("List Problems");
        scheduleMaintenance.setText("Schedule Maintenance");
        maintenanceCost.setText("Cost");
        problemRate.setText("Problem Rate");
        downtimeRate.setText("Downtime Rate");
                  
        fnostxt = new JTextField();
        lognumbertxt = new JTextField();
        problemtxt = new JTextField();
        conditiontxt = new JTextField();
        hidden_id3 = new JTextField();
        incidentreporttxt = new JTextField();
        expensetxt = new JTextField();
        repairtimetxt = new JTextField();
        repairtimetxt2 = new JTextField();
        
        fnostxt.setColumns(13);
        lognumbertxt.setColumns(13);
        problemtxt.setColumns(13);
        conditiontxt.setColumns(13);
        hidden_id3.setColumns(6);
        incidentreporttxt.setColumns(13);
        expensetxt.setColumns(13);
        repairtimetxt.setColumns(13);
        repairtimetxt2.setColumns(13);
        
        fnostxt.setEditable(false);
        lognumbertxt.setEditable(false);
        problemtxt.setEditable(false);
        conditiontxt.setEditable(false);
        hidden_id3.setVisible(false);
        hidden_id3.setEditable(false);
        incidentreporttxt.setEditable(false);
        expensetxt.setEditable(false);
        repairtimetxt.setEditable(false);
        repairtimetxt2.setEditable(false);
        
      
        jLabel8 = new JLabel();
        jLabel11 = new JLabel();
        jLabel12 = new JLabel();
        jLabel13 = new JLabel();
        jLabel14 = new JLabel();
        jLabel15 = new JLabel();
        jLabel16 = new JLabel();
                
        jLabel8.setText("Facility Number: ");
        jLabel11.setText("Log Number: ");
        jLabel12.setText("Problem: ");
        jLabel13.setText("Incident Report: ");
        jLabel14.setText("Condition: ");
        jLabel15.setText("Expense: ");
        jLabel16.setText("Repair Time: ");
        
        JPanel row13 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        row13.add(maekRequest);
        row13.add(listMaintenance);
        row13.add(maintenanceSelect);
        
        JPanel row14 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        row14.add(jLabel8);
        row14.add(fnostxt);
        row14.add(hidden_id3);
        JPanel row15 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        row15.add(jLabel11);
        row15.add(lognumbertxt);
        row15.add(jLabel12);
        row15.add(problemtxt);
        JPanel row16 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        row16.add(jLabel13);
        row16.add(incidentreporttxt);
        row16.add(jLabel14);
        row16.add(conditiontxt);
        JPanel row17 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        row17.add(jLabel15);
        row17.add(expensetxt);
        row17.add(jLabel16);
        row17.add(repairtimetxt);
        row17.add(repairtimetxt2);
        
        JPanel row18 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        row18.add(listFacilityProblems);
        row18.add(scheduleMaintenance);
        row18.add(maintenanceCost);
        row18.add(problemRate);
        row18.add(downtimeRate);
                
        maintenancePanel.add(row13);
        maintenancePanel.add(row14);
        maintenancePanel.add(row15);
        maintenancePanel.add(row16);
        maintenancePanel.add(row17);
        maintenancePanel.add(row18);
        
        maekRequest.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                makeRequestActionPerformed(e);
            }
        });
        listMaintenance.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                listMaintenanceActionPerformed(e);
            }
        });
        maintenanceSelect.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                maintenanceSelectActionPerformed(e);
            }
        });
        listMaintRequests.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                listMaintenanceRequestsActionPerformed(e);
            }
        });
        listFacilityProblems.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                listFacilityProblemsActionPerformed(e);
            }
        });
        scheduleMaintenance.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                scheduleMaintenanceActionPerformed(e);
            }
        });
        maintenanceCost.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                maintenanceCostActionPerformed(e);
            }
        });
        problemRate.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                problemRateActionPerformed(e);
            }
        });
        downtimeRate.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                downTimeRateActionPerformed(e);
            }
        });
        //</editor-fold>
        //End of Maintenance Panel
        bar.add("Login", loginPanel);
        bar.add("Facility", connectPanel);
        bar.add("Use", usePanel);
        bar.add("Maintenance", maintenancePanel);
        cards.add(bar, "Bar");
        
        frame.add(cards);
        frame.setSize(800, 400);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    private void connectActionPerformed(ActionEvent evt)
    {
         MyFunctions test = new MyFunctions();
                con = test.global_con();
                try{
                    if(con.isValid(con.getNetworkTimeout()))
                    {
                        enabled(true,false);
                        JOptionPane.showMessageDialog(null, "Connected.");
                        
                    }
                }
                //Do not modify 
                catch(Exception se)
                {}            
    }
    private void disconnectActionPerformed(ActionEvent evt) 
    {
        con = null;
        enabled(false, true);
        clearFields();
        JOptionPane.showMessageDialog(null, "Disconnected.");
    }
    private void addActionPerformed(ActionEvent evt) 
    {
        if (add.getText().equals("Add")) {
            add.setText("Save");
            clearFields();
        } else {
            String err = validated();
            if (!err.equals("")) {
                JOptionPane.showMessageDialog(null, err);
            } else {
                String exist = exist("");
                if (exist.equals("0")) {
                    try {
                       
                        
                        Statement st = con.createStatement();
                        if (faccapacitytxt.getText().equals("")) {
                            faccapacitytxt.setText("-1");
                        }
                        st.executeUpdate("Insert into facility (facility_number, facility_detail, facility_capacity) values ('"
                                + facnumbertxt.getText() + "','" + facdetailtxt.getText() + "','" + faccapacitytxt.getText() + "')");
                        
                        String starttmstmptxt = "0000-00-00T00:00:00";
                        String endtmstmptxt = "0000-00-00T00:00:00"; 
                        
                      
                        st.executeUpdate("Insert into usee (usef_number, start, end) values ('" + facnumbertxt.getText()+ "','"+ starttmstmptxt +"','" + endtmstmptxt + "')");
                         Inspections temporary = new Inspections();
                        temporary.rejumble();
                        st.executeUpdate("Insert into Inspections (inspectionsf_number, fireProtection, emergencyPreparedness, houseKeeping) values ('" + facnumbertxt.getText() + "','" + temporary.getFireProtection()+ "','" + temporary.getEmergencyPreparedness() + "','" + temporary.getHouseKeeping()+ "')");
		
                        
                        Usage t = new Usage();
                        st.executeUpdate("Insert into Usage2 (usagef_number,fee, item) values ('" + facnumbertxt.getText() + "','" + t.getFee() + "','" + t.getItem() + "')");
                        st.executeUpdate("Insert into maintenance values ('" + facnumbertxt.getText() + "')");
                        int tempLog = 0;
                        //check if the max log is 0 or null; if it is then establish tempLog = 1,
                        //if the max is 1, then set tempLog = whatever the max is +1; 
                        st.executeQuery("Select MAX(log) as log from Maintenance_Request");
                        ResultSet rs = st.getResultSet();
                        if(rs.next() == false)
                            JOptionPane.showMessageDialog(null, "No record found.");
                        else
                        {
                            rs.beforeFirst();
                            while(rs.next())
                            {
                              tempLog = rs.getInt("log");
                              System.out.println(tempLog);
                              if(rs.wasNull())
                              {
                                  tempLog = 1;
                              }
                              else if(tempLog == 0)
                              {
                                  tempLog = 1;
                              }
                              else
                              {
                                  tempLog = 1+tempLog;
                              }
                            }
                        }
                        st.executeUpdate("Insert into Maintenance_Request (maintenance_requestf_number,log) values ('" + facnumbertxt.getText() + "','" + tempLog + "')");
                        JOptionPane.showMessageDialog(null, "Record Saved.");
                        clearFields();

                        facnumbertxt.requestFocus();
                        add.setText("Add");
                    } catch (Exception se) {
                        JOptionPane.showMessageDialog(null, se.getMessage());
                    }
                } else if (exist.equals("1")) {
                    JOptionPane.showMessageDialog(null, "Facility Number already exists.");
                    facnumbertxt.requestFocus();
                }
            }
        }
    }
    private void searchActionPerformed(ActionEvent evt)
    {
        add.setText("Add");
        facdetailtxt.setText("");
        faccapacitytxt.setText("");
        hidden_id.setText("");
        if (facnumbertxt.getText().equals("")) 
        {
            JOptionPane.showMessageDialog(null, "Enter Facility Number");
            facnumbertxt.requestFocus();
        } else 
        {
            try 
            {
                Statement st = con.createStatement();
                st.executeQuery("Select * from facility where facility_number = '" + facnumbertxt.getText() + "'");
                ResultSet rs = st.getResultSet();

                if (rs.next() == false) 
                {
                    JOptionPane.showMessageDialog(null, "No record found.");
                    facnumbertxt.requestFocus();
                } 
                else 
                {
                    rs.beforeFirst();
                    while (rs.next()) 
                    {
                        facnumbertxt.setText(rs.getString("facility_number"));
                        faccapacitytxt.setText(rs.getString("facility_capacity"));
                        facdetailtxt.setText(rs.getString("facility_detail"));
                        hidden_id.setText(rs.getString("facility_number"));
                    }
                }
            } 
            catch (Exception se) 
            {
                JOptionPane.showMessageDialog(null, se.getMessage());
            }
        }
    }
    private void deleteActionPerformed(ActionEvent evt)
    {
        if (hidden_id.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Search the facility to be deleted.");
        } else {
            try {
                int answer = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this record?", "Message", JOptionPane.YES_NO_OPTION);
                if (answer == JOptionPane.YES_OPTION) {
                    Statement stmt = con.createStatement();
                    stmt.executeUpdate("Delete from facility where facility_number = '" + hidden_id.getText() + "'");
                    stmt.executeUpdate("Delete from usee where usef_number ='" + hidden_id.getText() + "'");
                    stmt.executeUpdate("Delete from Usage2 where usagef_number ='" + hidden_id.getText() + "'");
                    stmt.executeUpdate("Delete from maintenance where maintenancef_number ='" + hidden_id.getText() + "'");
                    stmt.executeUpdate("Delete from Inspections where inspectionsf_number ='" + hidden_id.getText() + "'");
                    JOptionPane.showMessageDialog(null, "Record Deleted.");
                    clearFields();
                    facnumbertxt.requestFocus();
                }
            } catch (Exception se) {
                JOptionPane.showMessageDialog(null, se.getMessage());
            }
        }
    }
    private void updateActionPerformed(ActionEvent evt) 
    {
        add.setText("Add");
        String err = validated();
        if (hidden_id.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Search the facility you want to update.");
        } else if (!err.equals("")) {
            JOptionPane.showMessageDialog(null, err);
        } else {
            String exist = exist("and facility_number != '" + hidden_id.getText() + "'");
            if (exist.equals("0")) {
                try {
                    if (faccapacitytxt.getText().equals("")) {
                        faccapacitytxt.setText("-1");
                    }
                    Statement st = con.createStatement();
                    st.executeUpdate("Update facility set facility_number = '" + facnumbertxt.getText() + "', facility_detail = '" + facdetailtxt.getText() + "', facility_capacity = '" + faccapacitytxt.getText() + "' where facility_number = '" + hidden_id.getText() + "'");
                    st.executeUpdate("Update usee set fa_number = '" + facnumbertxt.getText() + "' where facility_number ='"+hidden_id.getText()+"'");
                    JOptionPane.showMessageDialog(null, "Record Updated.");
                } catch (Exception se) {
                    JOptionPane.showMessageDialog(null, se.getMessage());
                    facnumbertxt.requestFocus();
                }
            } else {
                JOptionPane.showMessageDialog(null, "Facility Number already exists.");
                facnumbertxt.requestFocus();
            }
        }
    }
    private void clearActionPerformed(ActionEvent evt) {
        add.setText("Add");
        facnumbertxt.requestFocus();
        clearFields();
    }
    private void listFacilitiesActionPerformed(ActionEvent evt)
    {
        try
            {
                Statement st = con.createStatement();
                st.executeQuery("Select * from facility");
                ResultSet rs = st.getResultSet();
    
                String list = "";
    
                while(rs.next())
                    {
                        String facility_capacity = rs.getString("facility_capacity");
                        String facility_number = rs.getString("facility_number");
                        String facility_detail = rs.getString("facility_detail");
                
                        list = "Facility Number: " + facility_number + "\t\t" + "Facility Detail: " + facility_detail + "\t\t" + "Facility Capacity: " + facility_capacity 
                                + "\n" + list; 
                    }
                JTextArea textArea = new JTextArea(12, 70);
                textArea.setText(list);
                textArea.setEditable(false);
       
                // wrap a scrollpane around it
                JScrollPane scrollPane = new JScrollPane(textArea);
       
                // display them in a message dialog
                JOptionPane.showMessageDialog(null, scrollPane);
            }
            catch(Exception e)
                {
                    JOptionPane.showMessageDialog(null, e.getMessage());
                }
    }
    private void useClearActionPerformed(ActionEvent evt)
    {
        facnumbertxt.requestFocus();
        clearFields();
    }
    private void useDeleteActionPerformed(ActionEvent evt)
    {
        if(facnotxt.getText().equals(""))
            JOptionPane.showMessageDialog(null,"Search the facility to be deleted.");
        else
        {
            try
            {
               
                int answer = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this record?", "Message", JOptionPane.YES_NO_OPTION);
                if(answer == JOptionPane.YES_OPTION)
                {
                    Statement st = con.createStatement();
                    st.executeUpdate("Delete from facility where facility_number = '" + hidden_id2.getText() + "'");
                    st.executeUpdate("Delete from usee where usef_number = '" + hidden_id2.getText() + "'");
                    
                    clearFields();
                    JOptionPane.showMessageDialog(null, "Record Deleted.");
                    facnumbertxt.requestFocus();
                    
                }
            }
            catch(Exception e)
            {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }     
    }
    private void useSearchActionPerformed(ActionEvent evt)
    {
        add.setText("Add");
        vacanttxt.setText("");
        feetxt.setText("");
        starttxt.setText("");
        endtxt.setText("");
        hidden_id2.setText("");
        
        if(facnotxt.getText().equals(""))
        {
            JOptionPane.showMessageDialog(null,"Enter Facility Number.");
            facnotxt.requestFocus();
        }
        else
        {
            try
            {
                Statement st = con.createStatement();
                st.executeQuery("Select * from usee WHERE usef_number = '" + facnotxt.getText() + "'");
                ResultSet rs = st.getResultSet();
                
                if(rs.next() == false)
                {
                    JOptionPane.showMessageDialog(null, "No record found.");
                    facnotxt.requestFocus();
                }
                else
                {
                    rs.beforeFirst();
                    while(rs.next())
                    {
                        hidden_id2.setText(facnotxt.getText());
                        feetxt.setText(rs.getString("fee"));
                        vacanttxt.setText(rs.getString("vacant"));
                        
                        starttxt.setText(String.format("%1$TD %1$TT", rs.getTimestamp("start")));
                        endtxt.setText(String.format("%1$TD %1$TT", rs.getTimestamp("end")));
                      
                        
                    }
                }
            }
            catch(Exception e)
            {
              JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }
    private void vacateActionPerformed(ActionEvent evt)
    {        
        if(hidden_id2.getText().equals(""))
            JOptionPane.showMessageDialog(null, "Search the facility to be vacated.");
     
        else
        {
            vacanttxt.setText("True");
            try
            {
                Statement st = con.createStatement();
                st.executeUpdate("Update usee set vacant = 'True' where usef_number = '" + hidden_id2.getText() + "'");
                JOptionPane.showMessageDialog(null,"Record Updated.");
            }
            catch(Exception e)
            {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }
    private void assignFacilityActionPerformed(ActionEvent evt)
    {
        if(hidden_id2.getText().equals(""))
            JOptionPane.showMessageDialog(null, "Search the facility to be vacated.");
        else
        {
            vacanttxt.setText("False");
            try
            {
                Statement st = con.createStatement();
                st.executeUpdate("Update usee set vacant = 'False' where usef_number = '" + hidden_id2.getText() + "'");
                JOptionPane.showMessageDialog(null, "Record Updated.");
            }
            catch(Exception e)
            {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }
    private void useUpdateActionPerformed(ActionEvent evt)
    {
        if(hidden_id2.getText().equals(""))
            JOptionPane.showMessageDialog(null, "Search the facility you want to update.");
        else
        {
            try
            {
                Statement st = con.createStatement();
                st.executeUpdate("Update usee set usef_number ='" + facnotxt.getText() + "', fee = '" + feetxt.getText() + "', vacant = '" + vacanttxt.getText() + "', start ='" 
                        + starttxt.getText() + "' end = '" + endtxt.getText() + "' where usef_number = '" + hidden_id2.getText() + "'");
                JOptionPane.showMessageDialog(null,"Record Updated.");

            }
            catch(Exception e)
            {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
        
    }
    private void listInspectionsActionPerformed(ActionEvent evt)
    {
        try
            {
                Statement st = con.createStatement();
                st.executeQuery("Select * from Inspections");
                ResultSet rs = st.getResultSet();
    
                String list = "";
    
                while(rs.next())
                    {
                        String fire_protection = rs.getString("fireProtection");
                        String emergency_preparedness = rs.getString("emergencyPreparedness");
                        String house_keeping = rs.getString("houseKeeping");
                        String inspection_number = rs.getString("inspectionsf_number");
                        
                        list = "Facility Number: " + inspection_number + 
                                "\t\tEmergency Preparedness: " + emergency_preparedness + 
                                "\t\tHouse Keeping: " + house_keeping 
                                +"\t\tFire Protection: " + fire_protection + "\n" + list;
                    }
                JTextArea textArea = new JTextArea(12, 70);
                textArea.setText(list);
                textArea.setEditable(false);
       
                // wrap a scrollpane around it
                JScrollPane scrollPane = new JScrollPane(textArea);
       
                // display them in a message dialog
                JOptionPane.showMessageDialog(null, scrollPane);
            }
            catch(Exception e)
                {
                    JOptionPane.showMessageDialog(null, e.getMessage());
                }
        
    }
    public void listUsageActionPerformed(ActionEvent evt)
    {
        try
            {
                Statement st = con.createStatement();
                st.executeQuery("Select * from Usage2");
                ResultSet rs = st.getResultSet();
    
                String list = "";
    
                while(rs.next())
                    {
                        String usagef_number = rs.getString("usagef_number");
                        String item = rs.getString("item");
                        String fee = rs.getString("fee");
                        
                        list = "Facility Number: " + usagef_number + "\t\tItem: " + item + "\t\tFee: " + fee + "\n" + list;
                    }
                JTextArea textArea = new JTextArea(12, 70);
                textArea.setText(list);
                textArea.setEditable(false);
       
                // wrap a scrollpane around it
                JScrollPane scrollPane = new JScrollPane(textArea);
       
                // display them in a message dialog
                JOptionPane.showMessageDialog(null, scrollPane);
            }
            catch(Exception e)
                {
                    JOptionPane.showMessageDialog(null, e.getMessage());
                }
    }
    private void calcUsageRateActionPerformed(ActionEvent evt)
    {
        if(hidden_id2.getText().equals(""))
            JOptionPane.showMessageDialog(null,"Search a facility you want the calculated usage rate from.");
        else
        {
            try
            {
                Statement st = con.createStatement();
                st.executeQuery("Select * from Usage2 where usagef_number ='" + hidden_id2.getText() + "'");
                ResultSet rs = st.getResultSet();
                
                if(rs.next() == false)
                {
                    JOptionPane.showMessageDialog(null, "Record not found.");
                    facnotxt.requestFocus();
                }
                else
                {
                    rs.beforeFirst();
                    int total = 0;
                    String items = "";
                    while(rs.next())
                    {
                        int temporary = Integer.parseInt(rs.getString("fee"));
                        String temporary2 = rs.getString("item");
                        
                        items = temporary2 + items;
                        total = temporary + total;
                    }
                    JOptionPane.showMessageDialog(null,"Actual Usage Rate: $" + total + " a day. \n" +
                            "These are from appliances: " + items);
                }
            }
            catch(Exception e)
            {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }
    private void checkAvailableActionPerformed(ActionEvent evt)
    {
        if(hidden_id2.getText().equals(""))
            JOptionPane.showMessageDialog(null, "Search the facility you want to check the availability of.");
        else
        {
            try
            {
               //TODO compare datavalues with entered values 
                Statement st = con.createStatement();
                st.executeQuery("");
                ResultSet rs = st.getResultSet();
                
                if(rs.next() == false)
                {
                    JOptionPane.showMessageDialog(null,"Record not found.");
                    facnotxt.requestFocus();
                }
                else
                {
                    rs.beforeFirst();
                    Boolean checker = false; 
                    while(rs.next())
                    {
                        Timestamp start = rs.getTimestamp("start");
                        Timestamp end = rs.getTimestamp("end");
                        
                        String earliest = starttxt.getText();
                        String latest = starttxt.getText();
                        
                        Timestamp early = Timestamp.valueOf(earliest);
                        Timestamp late = Timestamp.valueOf(latest);
                        
                        if(early.before(start) && late.before(start))
                            checker = true; 
                        else if(early.after(end))
                            checker = true;
                        else
                            checker = false;
                    }
                    if(checker)
                        JOptionPane.showMessageDialog(null,"Facility Number: " + hidden_id2.getText() + "is not in use.");
                    else
                        JOptionPane.showMessageDialog(null, "Facility Number: " + hidden_id2.getText() + " is in use.");
                }   
            }
            catch(Exception e)
            {
                JOptionPane.showMessageDialog(null,e.getMessage());
                facnotxt.requestFocus();
            }
        }
    }
    private void makeRequestActionPerformed(ActionEvent evt)
    {
        if(fnostxt.getText().equals(""))
        {
            JOptionPane.showMessageDialog(null,"Search the facility you want to Maek Request");
            fnostxt.requestFocus();
        }
        else
        {
            try
            {
                Statement st = con.createStatement();
                st.executeUpdate("Update Maintenance_Request set log ='" + lognumbertxt.getText() + "', incident_report = '" + 
                        incidentreporttxt.getText() + "' condition = '" + conditiontxt.getText() + "', expense ='" + expensetxt.getText() +
                        "', startFix = '" + repairtimetxt.getText() + "', endFix = '" + repairtimetxt2.getText() 
                        + "' where maintenance_requestf_number = '"
                        + fnostxt.getText() + "'");
                JOptionPane.showMessageDialog(null, "Record Updated.");
            }
            catch(Exception e)
            {
                JOptionPane.showMessageDialog(null,e.getMessage());
            }
        }
        /*
         st.executeUpdate("Update usee set usef_number ='" + facnotxt.getText() + "', fee = '" + feetxt.getText() + "', vacant = '" + vacanttxt.getText() + "', start ='"
                        + starttxt.getText() + "' end = '" + endtxt.getText() + "' where usef_number = '" + hidden_id2.getText() + "'");
                JOptionPane.showMessageDialog(null, "Record Updated.");

        */

    }
    private void listMaintenanceActionPerformed(ActionEvent evt)
    {
        
    }
    private void maintenanceSelectActionPerformed(ActionEvent evt)
    {
        
    }
    private void scheduleMaintenanceActionPerformed(ActionEvent evt)
    {
        
    }
    private void maintenanceCostActionPerformed(ActionEvent evt)
    {
        
    }
    private void listMaintenanceRequestsActionPerformed(ActionEvent evt)
    {
        
    }
    private void problemRateActionPerformed(ActionEvent evt)
    {
        
    }
    private void downTimeRateActionPerformed(ActionEvent evt)
    {
        
    }
    private void listFacilityProblemsActionPerformed(ActionEvent evt)
    {
        
    }
    private void enabled(boolean type, boolean type2){
        
        row4.setVisible(type);
        row5.setVisible(type);
        hidden_id.setVisible(type);
        hidden_id2.setVisible(type);
        hidden_id3.setVisible(type);
         maekRequest.setEnabled(type);
        listMaintenance.setEnabled(type);
        maintenanceSelect.setEnabled(type);
        listMaintRequests.setEnabled(type);
        listFacilityProblems.setEnabled(type);
        scheduleMaintenance.setEnabled(type);
        maintenanceCost.setEnabled(type);
        problemRate.setEnabled(type);
        downtimeRate.setEnabled(type);
        available.setEnabled(type);
        useSearch.setEnabled(type);
        useUpdate.setEnabled(type);
        useSearch.setEnabled(type);
        useClear.setEnabled(type);
        useDelete.setEnabled(type);
        fnostxt.setEditable(type);
        lognumbertxt.setEditable(type);
        problemtxt.setEditable(type);
        conditiontxt.setEditable(type);
        hidden_id3.setEditable(type);
        incidentreporttxt.setEditable(type);
        expensetxt.setEditable(type);
        repairtimetxt2.setEditable(type);
        repairtimetxt.setEditable(type);
        endtxt.setEditable(type);
        starttxt.setEditable(type);
        feetxt.setEditable(type);
        hidden_id2.setEditable(type);
        vacanttxt.setEditable(type);
        facnotxt.setEditable(type);
        hidden_id.setEditable(type);
        facnumbertxt.setEditable(type);
        facdetailtxt.setEditable(type);
        faccapacitytxt.setEditable(type);
        calcUsageRate.setEnabled(type);
        listUsage.setEnabled(type);
        listInspections.setEnabled(type);
        vacate.setEnabled(type);
        assignFacility.setEnabled(type);
        isInUseDuringInterval.setEnabled(type);
        listFacilities.setEnabled(type);
        search.setEnabled(type);
        add.setEnabled(type);
        update.setEnabled(type);
        delete.setEnabled(type);
        clear.setEnabled(type);
        disconnect.setEnabled(type);
        connect.setEnabled(type2);
             
    }   
    public String validated(){
        String err = "";
        if(facnumbertxt.getText().equals("")){
            err = "Facility Number is required";
            facnumbertxt.requestFocus();
        } 
            return err;
    }
     public String exist(String param)
    {
        String ret = "0";
        try
          {
              Statement st = con.createStatement();
              st.executeQuery("Select * from facility where facility_number = '"+facnumbertxt.getText()+"' "+param);
              ResultSet rs = st.getResultSet();
              if(rs.next() == true)
              {
                  ret = "1";
              }
          }
          catch(Exception e)
          {
              ret = "error";
              JOptionPane.showMessageDialog(null,e.getMessage());
        }
        return ret;
    }

    private void clearFields() {
        facnumbertxt.setText("");
        facdetailtxt.setText("");
        faccapacitytxt.setText("");
        hidden_id.setText("");
        
        facnotxt.setText("");
        vacanttxt.setText("");
        hidden_id2.setText("");
        feetxt.setText("");
        starttxt.setText("");
        endtxt.setText("");
        facnumbertxt.requestFocus();
    }

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Screen().setVisible(true);
            }
        });
    }
    
    //Private components -- do not modify
    private JPanel row4, row5;
    private JButton connect, disconnect, add, delete, update, search, clear, listFacilities;
    private JButton isInUseDuringInterval, assignFacility, vacate, listInspections, listUsage, calcUsageRate, useSearch, useClear, useDelete, useUpdate;
    private JButton available, maekRequest, listMaintenance, maintenanceSelect, listMaintRequests, listFacilityProblems, scheduleMaintenance, maintenanceCost;
    private JButton problemRate, downtimeRate;
    private JLabel jLabel1, jLabel2, jLabel3;
    private JLabel jLabel4, jLabel5, jLabel6, jLabel7, jLabel8, jLabel9, jLabel10; 
    private JLabel jLabel11,jLabel12,jLabel13,jLabel14,jLabel15,jLabel16;
    private JTextField facnumbertxt, hidden_id, faccapacitytxt, facdetailtxt;
    private JTextField facnotxt, vacanttxt, feetxt, hidden_id2, starttxt, endtxt;
    private JTextField fnostxt, lognumbertxt, problemtxt, conditiontxt, hidden_id3, incidentreporttxt, expensetxt, repairtimetxt, repairtimetxt2; 
    private Connection con = null;
    //End of private components
}
