/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Daniel
 */
import java.time.LocalDateTime;
public class Maintenance_Schedule {
    private LocalDateTime getStartRepair(){
        return this.startRepair;
    }
    private void setStartRepair(LocalDateTime startRepair){
        this.startRepair = startRepair;
    }
    private LocalDateTime getEndRepair(){
        return this.endRepair;
    }
    private void setEndRepair(LocalDateTime endRepair){
        this.endRepair = endRepair;
    }
    public int getMNumber(){
        return this.mNumber;
    }
    public void setMNumber(int mNumber){
        this.mNumber = mNumber;
    }
    
    private LocalDateTime startRepair, endRepair;
    private int mNumber;
}
