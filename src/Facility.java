public class Facility implements IFacility {

	private int capacity;
	private int number;
	private Facility_Detail detail;
        private FacilityDAO bridge;

	@Override
	public Object listFacilities() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Object getFacilityInformation(int number) {
		Facility temporary = null; 
		temporary = bridge.getFacility(number); 
				
		String facilityInformation = "This is facility number: " + number + 
				"\n Capacity: " + capacity + 
				"\n Detail: " + temporary.getDetail().getCharacter(); 
		return facilityInformation;
	}
	@Override
	public Object requestAvailableCapacity(int number) {
		Facility temporary = null; 
		temporary = bridge.getFacility(number);
		
		int capacity = temporary.getCapacity();
		String capacityStringFormat = String.valueOf(capacity);
		return capacityStringFormat; 
	}
	
	//TODO think about had to change interface facility and add getter methods  to permit these methods to exist in 
	//the facilityDAO class 
	@Override
	public Object addNewFacility(IFacility newFacility) {
		bridge.addFacility(newFacility);
		return null;
	}
	@Override
	public void addFacilityDetail() {
		// TODO Auto-generated method stub

	}
	@Override
	public Object removeFacility(int number) {
		// TODO Auto-generated method stub
		return null;
	} 

	//getters and setters 
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public Facility_Detail getDetail() {
		return detail;
	}
	public void setDetail(Facility_Detail detail) {
		this.detail = detail;
	}
	public FacilityDAO getBridge() {
		return bridge;
	}
	public void setBridge(FacilityDAO bridge) {
		this.bridge = bridge;
	}


}
