public interface IMaintenanceRequest {

	public int getLog();
	public void setLog(int log);
	public String getIncidentReport();
	public void setIncidentReport(String incidentReport);

}