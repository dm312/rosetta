import java.time.LocalDateTime;
import java.time.temporal.TemporalAmount;

public class Maintenance_Request implements IMaintenanceRequest {

	
	public TemporalAmount getDo_you_know_how_long_it_takes() {
		return do_you_know_how_long_it_takes;
	}
	public void setDo_you_know_how_long_it_takes(
			TemporalAmount do_you_know_how_long_it_takes) {
		this.do_you_know_how_long_it_takes = do_you_know_how_long_it_takes;
	}
	public String getIncidentReport() {
		return incidentReport;
	}
	public void setIncidentReport(String incidenReport) {
		this.incidentReport = incidenReport;
	}
	public Condition getCondition() {
		return condition;
	}
	public void setCondition(Condition condition) {
		this.condition = condition;
	}
	public int getExpense() {
		return expense;
	}
	public void setExpense(int expense) {
		this.expense = expense;
	}
	public LocalDateTime getStartFix() {
		return startFix;
	}
	public void setStartFix(LocalDateTime startFix) {
		this.startFix = startFix;
	}
	public LocalDateTime getEndFix() {
		return endFix;
	}
	public void setEndFix(LocalDateTime endFix) {
		this.endFix = endFix;
	}
	public int getLog() {
		return log;
	}
	public void setLog(int log) {
		this.log = log;
	}
	public String toString(){
		return "Time required to fix: " + do_you_know_how_long_it_takes + 
				"\nIncident Issue Report: " + incidentReport + 
				"\nCondition: " + condition + 
				"\nExpense: " + expense + 
				"\nStart repair date: " + startFix + 
				"\nFinish repair date: " + endFix + 
				"\nIncident Log #: " + log; 

	}

	public static void main(String[] args){
		Maintenance_Request x = new Maintenance_Request();
		x.setCondition(Condition.BROKEN);
		System.out.println(x);
		System.out.println(x.toString());
	}
	public String getProblem() {
		return problem;
	}
	public void setProblem(String problem) {
		this.problem = problem;
	}
      
private TemporalAmount do_you_know_how_long_it_takes;
	private String problem;
	private String incidentReport; 
	private Condition condition;
	private int expense; 
	private LocalDateTime startFix, endFix; 
	private int log = -1; 
        private int maintenance_requestf_number;

}
