
import java.time.LocalDateTime;
import java.sql.Connection;

public class Use implements IFacilityUse {
	
	private int fee; 
	private Boolean vacant;
	private Inspections checks;
        private LocalDateTime start, end; 
        private FacilityDAO bridge;
        
        public static void main(String[] args){
           
        }
	@Override
	public Object isInUseDuringInterval(LocalDateTime earliest, LocalDateTime latest) 
        {
            if(earliest.isBefore(start) && latest.isBefore(start))
                return false;
            else if(earliest.isAfter(end))
                return false;
            return true;
        }

	@Override
	public Object assignFacilityToUse(int number) {
		Use temporary = bridge.getUse(number);
               return null;
        }
        public boolean switchVacancy(boolean original){
           return !original;
        }
	@Override
	public Object vacateFacility(int number) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object listInspections() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object listActualUsage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object calcUsageRate(int number) {
		// TODO Auto-generated method stub
		return null;
	}
	
	//getters and setters
	public int getFee() {
		return fee;
	}
	public void setFee(int fee) {
		this.fee = fee;
	}
	public Boolean getVacant() {
		return vacant;
	}
	public void setVacant(Boolean vacant) {
		this.vacant = vacant;
	}
	public Inspections getChecks() {
		return checks;
	}
	public void setChecks(Inspections checks) {
		this.checks = checks;
	}
	public void setBridge(FacilityDAO bridge){
            this.bridge = bridge;
        }
        public FacilityDAO getFacilityDAO(){
            return this.bridge;
        }
}
