public interface IFacility {

	public Object listFacilities(); 
	public Object getFacilityInformation(int number);
	public Object requestAvailableCapacity(int number);
	public Object addNewFacility(IFacility newFacility);
	public void addFacilityDetail();
	public Object removeFacility(int number);
	public int getNumber();
	public int getCapacity();
	public Object getDetail();

}