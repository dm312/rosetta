import java.util.ArrayList;
import java.time.LocalDateTime;
import javax.swing.JOptionPane;

public class Maintenance implements IFacilityMaintenance{

	@SuppressWarnings("unchecked")
	private ArrayList<Maintenance_Request> workBoard = new ArrayList<Maintenance_Request>(); 
	private FacilityDAO bridge; 
	private int fNumber;
        private int maintenanceNumber; 
        private int uNumber;

	@Override
	public Object makeFacilityMaintRequest() {
		Maintenance_Request suggestion = new Maintenance_Request(); 
		addSuggestion(suggestion);
                return this;
        }
	public void addSuggestion(Maintenance_Request mr){
		workBoard.add(mr);
	}
	@Override
	public Object scheduleMaintenance(int log) {
		//find maintenance_request with log number 
		//want the specific maintreq from workboard to insert the start dates 

		Maintenance_Request temporary = this.findLog(log);
		//where to put action listener that promps user for date 
		//I would say the view and when info is submitted it is set 
		//what information gets deleted when a log is set 
		LocalDateTime earliest;
		LocalDateTime latest; 

		int year = this.getUserInput("What year start?");
		int month = this.getUserInput("What month start?");
		int dayOfMonth = this.getUserInput("What day start?");
		int hour = this.getUserInput("What hour start?");
		int  minute = this.getUserInput("What minute start?");
		int second = this.getUserInput("What second start?");
		earliest = LocalDateTime.of(year, month, dayOfMonth, hour, minute, second);

		year = this.getUserInput("What year end?");
		month = this.getUserInput("What month end?");
		dayOfMonth = this.getUserInput("What day end?");
		hour = this.getUserInput("What hour end?");
		minute = this.getUserInput("What minute end?");
		second = this.getUserInput("What second end?");
		latest = LocalDateTime.of(year, month, dayOfMonth, hour, minute, second);

		temporary.setStartFix(earliest);
		temporary.setEndFix(latest);

		workBoard.remove(this.findLogIndex(log));
		workBoard.add(temporary);

		return temporary; 

	}
	public Maintenance_Request findLog(int log){
		try {
			for(Maintenance_Request m : workBoard)
				if(m.getLog() == log)
					return m;
		}
		catch(Exception e){
			System.out.println("Sources>>Model>>Maintenance>>Maintenance-->Maintenance_Request findLog() error. \n cannot "
					+ "find log");
			System.out.println(e.getCause());
		}
                return null;
	}
	public int findLogIndex(int log){
		try {
			for(int ii = 0; ii < workBoard.size(); ii++)
				if(workBoard.get(ii).getLog() == log)
					return ii;
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
                return -1;
	}
	@Override
	public Object calcMaintenanceCostForFacility(Facility faci) {
		int expensesForMaintenanceRequests = this.addMaintenanceExpenses();
		int expensesForMaintenanceServices; 
                return -1;
	}

	public int addMaintenanceExpenses(){
		int cost = 0;
		for(Maintenance_Request m : workBoard)
			cost += m.getExpense();
		return cost;
	}

	@Override
	public Object calcProblemRateForFacility(Facility faci) {

		int totalCompletedAndIncompleteMaintReq = workBoard.size();
		int totalWorkableQuality = this.addWorkableQuality();
		int numberOfProblems = this.addNumberOfProblems();
		double problemRate = (double)totalWorkableQuality/(double)(totalCompletedAndIncompleteMaintReq) * 100;
		return problemRate;

	}

	public int addNumberOfProblems(){
		int number = 0;
		for(Maintenance_Request m: workBoard)
			if(m.getProblem())
				number += 1;
		return number;
	}
	public int addWorkableQuality(){
		int inf = 0;
		for(Maintenance_Request m : workBoard)
			inf += m.getCondition().getUse();
		return inf;
	}
	@Override
	public Object calcDownTimeForFacility() {

		LocalDateTime startAllRepairs = LocalDateTime.of(0,0,0,0,0);

		for(Maintenance_Request m : workBoard){
			if(m.getProblem())
			{
				startAllRepairs.plus(m.getDo_you_know_how_long_it_takes());
			}
		}
		int years = startAllRepairs.getYear();
		int days = startAllRepairs.getDayOfYear() + years*365; 
		if(startAllRepairs.getHour() >= 12)
			days += 1;

		return days;
	}

	//- simple string of what requests have been put in
	@Override
	public Object listMaintRequests() {
		String line = ""; 
		for(Maintenance_Request m : workBoard)
			if(m.getProblem())
				line = line + m.getIncidentReport() + "\n";
		return line;
	}	

	//- what maintenance has already been done and

	@Override
	public Object listMaintenance() {
		String line = "";
		for(Maintenance_Request m : workBoard)
			if(!m.getProblem())
				line = line + m.getIncidentReport() + "\n";
		return line;
	}
	//- what are all the problems (maintained or not) related to the facility.
	@Override
	public Object listFacilityProblems() {
		String line = ""; 
		for(Maintenance_Request m : workBoard)
			if(m.getProblem())
				line = line + m.getIncidentReport() + "\n";
		return line;
	}

	public ArrayList<Maintenance_Request> getWorkBoard() {
		return workBoard;
	}
	public void setWorkBoard(ArrayList<Maintenance_Request> workBoard) {
		this.workBoard = workBoard;
	}
	public int getFNumber() {
		return fNumber;
	}
	public void setFNumber(int fNumber) {
		this.fNumber = fNumber;
	}
	public FacilityDAO getBridge() {
		return bridge;
	}
	public void setBridge(FacilityDAO bridge) {
		this.bridge = bridge;
	} 
	public int getUserInput(String message) {
		String input = JOptionPane.showInputDialog(null, message);
		int asinteger = Integer.parseInt(input);
		return asinteger;
	}
        public int getMaintenanceNumber(){
            return this.maintenanceNumber;
        }
        public void setMaintenanceNumber(int maintenanceNumber){
            this.maintenanceNumber = maintenanceNumber;
        }
        public int getUNumber(){
            return this.uNumber;
        }
        public void setUNumber(int uNumber){
            this.uNumber = uNumber;
        }
	public static void main(String[] args){

	}
}