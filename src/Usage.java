/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Daniel
 */
import java.util.*;
public class Usage {
    private int usagef_number, fee;
    private String item;
    ArrayList<String> bag = new ArrayList<String>();
    Random generator = new Random();
    
    public Usage()
    {
        bag.add("Washing Machine");
        bag.add("Arcade");
    
        int randomInteger = generator.nextInt(88888);
        if(randomInteger >= 4444)
        {
            item = bag.get(0);
            fee = 25;
        }
        else
        {
            item = bag.get(1);
            fee = 10;
        }
        
    }
    public int getUsagef_number()
    {
        return this.usagef_number;
    }
    public int getFee()
    {
        return this.fee;
    }
    public String getItem()
    {
        return this.item;
    }

}

