public enum Condition {
	NEW (100), FINE (60), OLD (40), BROKEN(10);

	private final int use; 
	Condition(int use){
		this.use = use;
	}
	public int getUse() {
		return use;
	}
        public int getMNumber(){
            return this.mNumber;
        }
        public void setMNumber(int mNumber){
            this.mNumber = mNumber;
        }
	private int mNumber;
}
