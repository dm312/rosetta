import java.time.LocalDateTime;

public interface IFacilityUse {

	public Object isInUseDuringInterval(LocalDateTime earliest, LocalDateTime latest);
	public Object assignFacilityToUse(int number);
	public Object vacateFacility(int number);
	public Object listInspections();
	public Object listActualUsage();
	public Object calcUsageRate(int number);
	
}